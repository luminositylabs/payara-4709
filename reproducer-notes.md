# Payara-4709 Reproducer Notes (https://github.com/payara/Payara/issues/4709)

This project can be used to reproduce/illustrate an apparent bug that exists in Payara in which the "Secrets Directory"
microprofile config provider appears not to be used when dereferencing MPCONFIG parameters in a web.xml descriptor for a
deployed web module.  The project is set up with multiple maven profiles along with parameterized docker-compose.xml and
Dockerfile files allowing a specific Payara version between 5.192 and 5.2020.6 (inclusive) to be chosen.

## Project Structure

The project contains a parent module with a submodule named web1.  The web1 submodule produces a war file which can then
be used in a docker image produced via the docker-compose.xml and associated files in the docker directory.

### Building the WAR

The deployable war file can be built from the web1 directory by invoking maven with or without specifying a profile.  If
a profile is not specified, the default is to build for Payara v5.192:
```
 mvn clean package
```

To build for specific versions, one of the following profiles may be specified:
```
 mvn -Ppayara-v5.192 clean package
 mvn -Ppayara-v5.193 clean package
 mvn -Ppayara-v5.193.1 clean package
 mvn -Ppayara-v5.194 clean package
 mvn -Ppayara-v5.201 clean package
 mvn -Ppayara-v5.2020.2 clean package
 mvn -Ppayara-v5.2020.3 clean package
 mvn -Ppayara-v5.2020.4 clean package
 mvn -Ppayara-v5.2020.5 clean package
 mvn -Ppayara-v5.2020.6 clean package
```

### Building and running the services

After building the war file with maven, docker-compose can then be used to build and run the services.  The README.md
file in the base directory of the project shows how to invoke docker-compose to start and stop the services.

The docker-compose.yml file declares two services along with a secret.  The first service (db) is for the database
and uses a prebuilt postgres docker image, while the second service (webapp) is for the Payara server and is built
locally by inheriting from the official Payara docker image and adding a custom post-boot command file.  In addition to
these two services, a "secret" is also declared which is a resource used to specify the database password to both the
database and the appserver.  The secret is referenced in both services which results in the
`./docker/secrets/MPCONFIGAPP1_DB_PASSWORD` file being mounted into the containers in the `/run/secrets` directory
during runtime.  This file is a simple text file containing the database password and is read by postgres startup
scripts defined in the database container to initialize the authentication information for the database.  For the
Payara container, the file is ostensibly read by the Payara microprofile config secrets directory provider. 

While the db service uses a prebuilt container image, the webapp service uses an image which is built locally.
The image is built from the `./docker/images/webapp/Dockerfile-webapp` file which copies the war file built by maven
as well as the `./docker/images/webapp/post-boot-commands.asadmin` file into the image.  The war file is intentionally
NOT copied into the deployment directory in order to avoid automatically deploying it.  Instead, the
post-boot-commands.asadmin file contains a command to deploy the war file with a specific context root.  The file also
contains a command to set the Payara config directory to the /run/secrets directory where the secrets microprofile
config provider will read the secrets from.

When the services are started via docker-compose, the web1 war should be deployed into Payara container and the results
of the deployment should be shown in the webapp_1 service logs.  A successful startup and deployment should result
in a message near the end of the logs stating the webapp was successfully deployed along with the time it took to
deploy.  Additionally, the db_1 service logs may show an error with a message stating that the "testentity" relation
does not exist, but this is a positive indication that the deployment succeeded and that the webapp was able to contact
the database.  A failure to connect to the database will be shown as jdbc errors about password authentication
accompanied by stack traces or something similar.

On successful deployment and startup of the services, three servlets will be available via port 8080 at the following
endpoints:
```
 http://localhost:8080/configTestServlet
 http://localhost:8080/jdbcTestServlet
 http://localhost:8080/jpaTestServlet
```

The ConfigTestServlet first prints a list of system properties and environment variables obtained via the respective
java.lang.System methods.  It then iterates through all microprofile config providers, printing out each property name
and value from each provider.  Finally, it iterates through the global list of all properties and prints the name and
values for those properties as well.

The JdbcTestServlet opens a connection against an injected jdbc datasource, issues a query, then writes the results to
the servlet response writer.

The JpaTestServlet uses an injected entity manager to create a new record and then query all records of that type.  The
results of the query are written to the servlet response writer.  Repeatedly reloading the URL should show a new record
has been added with each reload.

Both the maven build and the docker-compose files have been parameterized to allow the Payara version to be specified
when building, deploying, and starting the services.  By default, version 5.192 is used, but the following shell
commands can be used to override the default:
```
   mvn -Ppayara-v5.192    clean package && PAYARA_VERSION=5.192    docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.193    clean package && PAYARA_VERSION=5.193    docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.193.1  clean package && PAYARA_VERSION=5.193.1  docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.194    clean package && PAYARA_VERSION=5.194    docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.201    clean package && PAYARA_VERSION=5.201    docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.2020.2 clean package && PAYARA_VERSION=5.2020.2 docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.2020.3 clean package && PAYARA_VERSION=5.2020.3 docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.2020.4 clean package && PAYARA_VERSION=5.2020.4 docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.2020.5 clean package && PAYARA_VERSION=5.2020.5 docker-compose -f docker/docker-compose.yml up --build
   mvn -Ppayara-v5.2020.6 clean package && PAYARA_VERSION=5.2020.6 docker-compose -f docker/docker-compose.yml up --build
```

## The problem:

**UPDATE**: It appears the issue has been fixed between versions 5.2020.3 and 5.2020.4

When using Payara v5.192 everything works fine.  The webapp docker image is built, the services are started, the web
application is deployed in the Payara container, and the servlets are able to access the database via the datasource
that is defined in the web.xml descriptor.

The problem comes into play when using one of the newer Payara versions.  Using v5.193 or newer, the docker image is
built, the services are started, but the web application deployment fails with a database authentication error.  It
seems the datasource defined in the web.xml can not dereference the MPCONFIG variable for the password during deployment
 in versions 5.193 and newer.  BUT, if the docker-compose.yml is changed to uncomment the environment variable specified
in the webapp service for the MPCONFIGAPP1_DB_PASSWORD variable, then deployment works.  It seems as if the secrets
provider isn't available during deployment, but the environment variable provider is.



The other detail to note is that this sample can be modified to deploy successfully on the newer Payara versions by
commenting out the <jta-data-source> element in persistence.xml and the @Resource annotation on the Datasouce field
of the DataSourceTestServlet class.  When these items are commented out, the application should deploy and the
ConfigTestServlet will show that the secrets directory provider DOES exist, and the secret is accessible.  This shows
that the secret is eventually available to the app that is deployed, but for some reason it's not available during
deployment for dereferencing the MPCONFIG variables in web.xml descriptor.
