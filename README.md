# PAYARA MICROPROFILE PROPERTY REFERENCES WITH SECRETS CONFIG PROVIDER

## Building

This project uses maven, docker, and docker-compose.

To build and deploy the project, invoke the following command line from the **web1** project dir:
```
mvn clean package && docker-compose -f docker/docker-compose.yml up --build
```

To shutdown the services, invoke the following command line from the **web1** project dir:
```
docker-compose -f docker/docker-compose.yml down -v
```

After subsequent project builds/deploys, many docker images for the project may accumulate.
A simple `docker image prune` from time to time can reduce the space used byb the accumulated docker images.

See [reproducer-notes.md](reproducer-notes.md) for details on reproducing the issue.
