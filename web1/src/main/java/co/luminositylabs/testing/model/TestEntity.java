package co.luminositylabs.testing.model;


import org.eclipse.persistence.annotations.CacheIndex;
import org.eclipse.persistence.annotations.UuidGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;


/** TestEntity persistent entity class. */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "TestEntity.findAll",
                query = "select te from TestEntity te"
        ),
        @NamedQuery(
                name = "TestEntity.findAllOrderedByCreateDate",
                query = "select te from TestEntity te order by te.createDate"
        ),
        @NamedQuery(
                name = "TestEntity.findAllOrderedByName",
                query = "select te from TestEntity te order by te.name"
        ),
        @NamedQuery(
                name = "TestEntity.findTotalCount",
                query = "select count(te) from TestEntity te"
        ),
        @NamedQuery(
                name = "TestEntity.findById",
                query = "select te from TestEntity te where te.id = :id"
        ),
        @NamedQuery(
                name = "TestEntity.findByName",
                query = "select te from TestEntity te where te.name = :name"
        ),
        @NamedQuery(
                name = "TestEntity.findByNames",
                query = "select te from TestEntity te where te.name in :names"
        )
})
public class TestEntity implements Serializable {

    /** Max length for name. */
    private static final int NAME_MAX_LENGTH_64 = 64;

    /** Max length for description. */
    private static final int DESCRIPTION_MAX_LENGTH_256 = 256;

    private static final long serialVersionUID = -8736847805708919948L;

    /** Entity primary key property. */
    @UuidGenerator(name = "TestEntityUUIDGenerator")
    @Id
    @GeneratedValue(generator = "TestEntityUUIDGenerator")
    private String id;

    /** The name. */
    @CacheIndex
    @Column(unique = true, nullable = false, length = NAME_MAX_LENGTH_64)
    private String name;

    /** The description. */
    @Column(length = DESCRIPTION_MAX_LENGTH_256)
    private String description;

    /** entity creation date property. */
    private OffsetDateTime createDate;

    /** entity most recent update date property. */
    private OffsetDateTime updateDate;

    /** entity version property. */
    @Version
    private long version;

    /** entity cached hashcode property. */
    @Transient
    private Integer cachedHashCode;


    /** Instantiates the entity with default state. */
    public TestEntity() {
        setCreateDate(OffsetDateTime.now());
    }


    /**
     * Instantiates the entity with the specified state.
     *
     * @param name The name to instantiate with
     */
    public TestEntity(final String name) {
        this();
        this.name = name;
    }


    /**
     * Instantiates the entity with the specified state.
     *
     * @param name The name to instantiate with
     * @param description The description to instantiate with
     */
    public TestEntity(final String name, final String description) {
        this(name);
        this.description = description;
    }


    /**
     * Gets the id.
     *
     * @return the id
     *
     */
    public String getId() {
        return id;
    }


    /**
     * Sets the id.
     *
     * @param id the id
     */
    public void setId(final String id) {
        this.id = id;
    }


    /**
     * Gets the name.
     *
     * @return the name
     *
     */
    public String getName() {
        return name;
    }


    /**
     * Sets the name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }


    /**
     * Gets the description.
     *
     * @return the description
     *
     */
    public String getDescription() {
        return description;
    }


    /**
     * Sets the description.
     *
     * @param description the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }


    /**
     * Gets the create date.
     *
     * @return the create date
     *
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }


    /**
     * Sets the create date.
     *
     * @param createDate the create date
     */
    public void setCreateDate(final OffsetDateTime createDate) {
        this.createDate = createDate;
    }


    /**
     * Gets the update date.
     *
     * @return the update date
     *
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }


    /**
     * Sets the update date.
     *
     * @param updateDate the update date
     */
    public void setUpdateDate(final OffsetDateTime updateDate) {
        this.updateDate = updateDate;
    }


    /**
     * Gets the version.
     *
     * @return the version
     *
     */
    public long getVersion() {
        return version;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getName());
        sb.append("{id=").append(getId());
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", description='").append(getDescription()).append('\'');
        sb.append(", createDate=").append(getCreateDate());
        sb.append(", updateDate=").append(getUpdateDate());
        sb.append(", version=").append(getVersion());
        sb.append(", cachedHashCode=").append(cachedHashCode);
        sb.append('}');
        return sb.toString();
    }


    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestEntity)) {
            return false;
        }
        TestEntity that = (TestEntity)o;
        return Objects.equals(getId(), that.getId())
                && Objects.equals(getName(), that.getName())
                && Objects.equals(getDescription(), that.getDescription())
                && Objects.equals(getCreateDate(), that.getCreateDate())
                && Objects.equals(getUpdateDate(), that.getUpdateDate());
    }


    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    public int hashCode() {
        if (cachedHashCode == null) {
            cachedHashCode = Objects.hash(
                    getName(),
                    getDescription(),
                    getCreateDate(),
                    getUpdateDate()
            );
        }
        return cachedHashCode;
    }


}
