package co.luminositylabs.testing;


import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigSource;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.stream.StreamSupport;


@WebServlet(name = "ConfigTestServlet", urlPatterns = {"/configTestServlet"})
public class ConfigTestServlet extends HttpServlet {

    private static final long serialVersionUID = -4895468932168635714L;

    @Inject
    private Config config;


    @Override
    public void init(final ServletConfig config) throws ServletException {
        System.out.println("*** initializing " + getClass().getName());
        super.init(config);
    }


    @Override
    protected void doGet(final HttpServletRequest httpServletRequest,
                         final HttpServletResponse httpServletResponse) throws IOException {
        final StringBuilder servletOutputBuilder =
                new StringBuilder("<html><head><title>").append(getClass().getSimpleName()).append("</title></head>\n")
                        .append("<h1>").append(getClass().getSimpleName()).append("</h1>\n");

        servletOutputBuilder.append("<br/>\n").append(renderSystemProperties()).append("<br/>\n");

        servletOutputBuilder.append("<br/>\n").append(renderEnvironmentProperties()).append("<br/>\n");

        if (config == null) {
            servletOutputBuilder.append("config was null (maybe CDI isn't activated? check beans.xml)<br/>\n");
        } else {
            if (config.getConfigSources() == null) {
                servletOutputBuilder.append("configSources was null (maybe CDI isn't activated? check beans.xml)<br/>\n");
            } else {
                servletOutputBuilder.append("<br/>\n").append(renderConfigsByConfigSource()).append("<br/>\n");
            }
            servletOutputBuilder.append("<br/>\n").append(renderConfig()).append("<br/>\n");
        }

        servletOutputBuilder.append("</body></html>\n");
        httpServletResponse.setContentType("text/html");
        try (PrintWriter printWriter = httpServletResponse.getWriter()) {
            printWriter.println(servletOutputBuilder.toString());
        }
    }


    private String renderConfig() {
        Objects.requireNonNull(config);
        final StringBuilder stringBuilder = new StringBuilder("<h2>Config (all config sources)</h2>\n");
        StreamSupport.stream(config.getPropertyNames().spliterator(), false)
                .sorted()
                .forEach(
                        e -> stringBuilder.append("&nbsp;")
                                .append(e)
                                .append(" => ")
                                .append(config.getOptionalValue(e, String.class).orElse(null))
                                .append("<br/>\n")
                );
        return stringBuilder.toString();
    }


    private String renderConfigsByConfigSource() {
        Objects.requireNonNull(config);
        Objects.requireNonNull(config.getConfigSources());
        final StringBuilder stringBuilder = new StringBuilder("<h2>Configs by Config Source</h2>\n");
        StreamSupport.stream(config.getConfigSources().spliterator(), false)
                .sorted(Comparator.comparing(ConfigSource::getOrdinal))
                .forEach(
                        cs -> {
                            stringBuilder.append("<br/>\n*** Name(Ordinal) *** ")
                                    .append(cs.getName())
                                    .append("/")
                                    .append(cs.getOrdinal())
                                    .append("<br/>\n");
                            cs.getProperties()
                                    .entrySet()
                                    .stream()
                                    .sorted(Map.Entry.comparingByKey())
                                    .forEach(
                                            e -> stringBuilder.append("&nbsp;")
                                                    .append(e.getKey())
                                                    .append(" => ")
                                                    .append(e.getValue())
                                                    .append("<br/>\n")
                                    );
                        }
                );
        return stringBuilder.toString();
    }


    private String renderSystemProperties() {
        final StringBuilder stringBuilder = new StringBuilder("<h2>SYSTEM PROPERTIES</h2>\n");
        System.getProperties()
                .entrySet()
                .stream()
                .sorted(Comparator.comparing(Object::toString))
                .forEach(e -> stringBuilder.append(e.getKey()).append(" => ").append(e.getValue()).append("<br/>\n"));
        return stringBuilder.toString();
    }


    private String renderEnvironmentProperties() {
        final StringBuilder stringBuilder = new StringBuilder("<h2>ENVIRONMENT PROPERTIES</h2>\n");
        System.getenv()
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(e -> stringBuilder.append(e.getKey()).append(" => ").append(e.getValue()).append("<br/>\n"));
        return stringBuilder.toString();
    }


    @Override
    protected void doPost(final HttpServletRequest httpServletRequest,
                          final HttpServletResponse httpServletResponse) throws IOException {
        doGet(httpServletRequest, httpServletResponse);
    }


    @Override
    public void destroy() {
        System.out.println("*** destroying " + getClass().getName());
        super.destroy();
    }


}
