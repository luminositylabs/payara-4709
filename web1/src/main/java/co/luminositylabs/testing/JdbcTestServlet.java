package co.luminositylabs.testing;


import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


@WebServlet(name = "JdbcTestServlet", urlPatterns = {"/jdbcTestServlet"})
public class JdbcTestServlet extends HttpServlet {

    private static final long serialVersionUID = 6961741237188070480L;

    @Resource(lookup = "java:global/TestDataSource")
    private DataSource dataSource;


    @Override
    public void init(final ServletConfig config) throws ServletException {
        System.out.println("*** initializing " + getClass().getName());
        super.init(config);
    }


    @Override
    protected void doGet(final HttpServletRequest httpServletRequest,
                         final HttpServletResponse httpServletResponse) throws IOException {
        final StringBuilder servletOutputBuilder =
                new StringBuilder("<html><head><title>").append(getClass().getSimpleName()).append("</title></head>\n")
                        .append("<h1>").append(getClass().getSimpleName()).append("</h1>\n");

        if (dataSource == null) {
            servletOutputBuilder.append("datasource was null<br/>\n");
        } else {
            try (
                    Connection connection = dataSource.getConnection();
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery("select 'OU812'")
            ) {
                if (resultSet.next()) {
                    servletOutputBuilder.append("result string: ")
                            .append(resultSet.getString(1))
                            .append("<br/>\n");
                } else {
                    servletOutputBuilder.append("there was no result<br/>\n");
                }
            } catch (Exception e) {
                servletOutputBuilder.append("Caught exception attempting to execute query:")
                        .append(e.getClass().getName())
                        .append(":")
                        .append(e.getMessage())
                        .append("<br/>\n");
            }
        }

        servletOutputBuilder.append("</body></html>\n");
        httpServletResponse.setContentType("text/html");
        try (PrintWriter printWriter = httpServletResponse.getWriter()) {
            printWriter.println(servletOutputBuilder.toString());
        }
    }


    @Override
    protected void doPost(final HttpServletRequest httpServletRequest,
                          final HttpServletResponse httpServletResponse) throws IOException {
        doGet(httpServletRequest, httpServletResponse);
    }


    @Override
    public void destroy() {
        System.out.println("*** destroying " + getClass().getName());
        super.destroy();
    }


}
