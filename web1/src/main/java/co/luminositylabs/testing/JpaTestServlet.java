package co.luminositylabs.testing;


import co.luminositylabs.testing.model.TestEntity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;


@WebServlet(name = "JpaTestServlet", urlPatterns = {"/jpaTestServlet"})
public class JpaTestServlet extends HttpServlet {

    private static final long serialVersionUID = 325954215106418972L;

    /** The entity manager. */
    @PersistenceContext
    private EntityManager entityManager;

    /** The tx manager. */
    @Inject
    private UserTransaction userTransaction;


    @Override
    public void init(final ServletConfig config) throws ServletException {
        System.out.println("*** initializing " + getClass().getName());
        super.init(config);
    }


    @Override
    protected void doGet(final HttpServletRequest httpServletRequest,
                         final HttpServletResponse httpServletResponse) throws IOException {
        final StringBuilder servletOutputBuilder =
                new StringBuilder("<html><head><title>").append(getClass().getSimpleName()).append("</title></head>\n")
                        .append("<h1>").append(getClass().getSimpleName()).append("</h1>\n");

        if (entityManager == null) {
            servletOutputBuilder.append("entity manager was null<br/>\n");
        }
        if (userTransaction == null) {
            servletOutputBuilder.append("user transaction was null<br/>\n");
        }

        if ((entityManager != null) && (userTransaction != null)) {
            try {
                userTransaction.begin();
                entityManager.persist(new TestEntity("test_" + UUID.randomUUID()));
                userTransaction.commit();

                entityManager.createNamedQuery("TestEntity.findAll", TestEntity.class)
                        .getResultStream()
                        .forEach( te ->
                                servletOutputBuilder.append("test entity: ")
                                        .append(te.getId())
                                        .append(" / ")
                                        .append(te.getName()).append("<br/>\n")
                        );
            } catch (Exception e) {
                servletOutputBuilder.append("Caught exception attempting to persist an entity:")
                        .append(e.getClass().getName())
                        .append(":")
                        .append(e.getMessage())
                        .append("<br/>\n");
            }
        }

        servletOutputBuilder.append("</body></html>\n");
        httpServletResponse.setContentType("text/html");
        try (PrintWriter printWriter = httpServletResponse.getWriter()) {
            printWriter.println(servletOutputBuilder.toString());
        }
    }


    @Override
    protected void doPost(final HttpServletRequest httpServletRequest,
                          final HttpServletResponse httpServletResponse) throws IOException {
        doGet(httpServletRequest, httpServletResponse);
    }


    @Override
    public void destroy() {
        System.out.println("*** destroying " + getClass().getName());
        super.destroy();
    }


}
